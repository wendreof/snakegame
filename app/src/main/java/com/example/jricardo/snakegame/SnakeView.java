package com.example.jricardo.snakegame;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.SoundPool;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.Random;

/**
 * Created by JRicardo on 20/10/2017.
 */

public class SnakeView extends SurfaceView implements Runnable {


    // O código será executado separadamente da UI
    // por isto é definida uma Thread
    private Thread m_Thread = null;

    // Esta variável determina quando o Snake Game está sendo jogado
    // Ela é declarada como volátil porque pode ser acessada
    // de dentro e de fora da thread
    private volatile boolean m_Playing;

    // Utilizaremos o Canvas para desenhar
    private Canvas m_Canvas;
    // O SurfaceHolder é requerido para que a classe Canvas faça o desenho
    private SurfaceHolder m_Holder;
    // Isto permite controlar cor, tamanho de texto, etc...
    private Paint m_Paint;

    // Esta será uma referência à Activity
    private Context m_context;

    // Sons
    private SoundPool m_SoundPool;
    private int m_get_apple_sound = -1;
    private int m_dead_sound = -1;

    // Para rastrear o movimento m_Direction
    public enum Direction {UP, RIGHT, DOWN, LEFT}
    // Inicia movendo para a direita
    private Direction m_Direction = Direction.RIGHT;

    // Guarda a resolução da tela
    private int m_ScreenWidth;
    private int m_ScreenHeight;

    // Controla a pausa entre as atualizações
    private long m_NextFrameTime;
    // Atualiza o jogo 10 vezes por segundo
    private final long FPS = 10;
    // Define o segundo como tendo mil milésimos de segundo
    private final long MILLIS_IN_A_SECOND = 1000;

    // Pontuação
    private int m_Score;

    // Localização no grid de todos os seguimentos da cobra
    private int[] m_SnakeXs;
    private int[] m_SnakeYs;

    // Tamanho da cobra
    private int m_SnakeLength;

    // Onde está a maçã
    private int m_AppleX;
    private int m_AppleY;

    // O tamanho em pixels do segmento da cobra
    private int m_BlockSize;

    // O tamanho em seguimentos da área de jogo
    private final int NUM_BLOCKS_WIDE = 40;
    private int m_NumBlocksHigh; // determinado dinamicamente


    public SnakeView(Context context, Point size) {

        super(context);

        m_context = context;

        m_ScreenWidth = size.x;
        m_ScreenHeight = size.y;

        //Determina o tamanho de cada bloco na tela do jogo
        m_BlockSize = m_ScreenWidth / NUM_BLOCKS_WIDE;
        // Número de blocos do mesmo tamanho distribuídos na altura
        m_NumBlocksHigh = ((m_ScreenHeight)) / m_BlockSize;

        // Liga o som
        loadSound();

        // Inicializa o desenho dos objetos
        m_Holder = getHolder();
        m_Paint = new Paint();

        // Se chegar a 200, o jogador é recompensado com uma colisão
        m_SnakeXs = new int[200];
        m_SnakeYs = new int[200];

        // Inicia o Jogo
        startGame();
    }

    @Override
    public void run() {
        // A verificação de m_Playing impede um erro ao iniciar o jogo
        // Você também pode alterar o código para fornecer um recurso de pausa
        while (m_Playing) {

            // Atualiza o jogo 10 vezes por segundo
            if(checkForUpdate()) {
                updateGame();
                drawGame();
            }

        }
    }

    public void loadSound() {
        m_SoundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        try {

            AssetManager assetManager = m_context.getAssets();
            AssetFileDescriptor descriptor;

            // Prepara os sons na memória
            descriptor = assetManager.openFd("sound_03.ogg");
            m_get_apple_sound = m_SoundPool.load(descriptor, 0);

            descriptor = assetManager.openFd("sound_02.ogg");
            m_dead_sound = m_SoundPool.load(descriptor, 0);


        } catch (IOException e) {
            // Erro
        }
    }

    public void startGame() {
        // Inicia o jogo apenas com a cabeça da cobra no centro da tela
        m_SnakeLength = 20;
        m_SnakeXs[0] = NUM_BLOCKS_WIDE / 2;
        m_SnakeYs[0] = m_NumBlocksHigh / 2;

        // Cria a maçã
        spawnApple();

        // Inicia o Score zerado
        m_Score = 0;

        // Configura o m_NextFrameTime para desencadear a atualização imediatamente
        m_NextFrameTime = System.currentTimeMillis();
    }

    public void spawnApple() {
        Random random = new Random();
        m_AppleX = random.nextInt(NUM_BLOCKS_WIDE - 1) + 1;
        m_AppleY = random.nextInt(m_NumBlocksHigh - 1) + 1;
    }

    private void eatApple(){
        //  Comeu a maçã!
        // Incrementa o tamanho da cobra
        m_SnakeLength++;
        //troca a maça de lugar
        spawnApple();
        //incrementa o m_Score
        m_Score = m_Score + 1;
        m_SoundPool.play(m_get_apple_sound, 1, 1, 0, 0, 1);
    }

    private void moveSnake(){
        // Move o corpo
        for (int i = m_SnakeLength; i > 0; i--) {
            // Inicia de trás para frente e move para o próximo segmento
            m_SnakeXs[i] = m_SnakeXs[i - 1];
            m_SnakeYs[i] = m_SnakeYs[i - 1];

            // Desconsidera a cabeça pois não tem ninguém na frente dela
        }

        // Move a cabeça na direção informada por m_Direction
        switch (m_Direction) {
            case UP:
                m_SnakeYs[0]--;
                break;

            case RIGHT:
                m_SnakeXs[0]++;
                break;

            case DOWN:
                m_SnakeYs[0]++;
                break;

            case LEFT:
                m_SnakeXs[0]--;
                break;
        }
    }

    private boolean detectDeath(){
        // A cobra morreu?
        boolean dead = false;

        // Acabou em uma parede?
        if (m_SnakeXs[0] == -1) dead = true;
        if (m_SnakeXs[0] >= NUM_BLOCKS_WIDE) dead = true;
        if (m_SnakeYs[0] == -1) dead = true;
        if (m_SnakeYs[0] == m_NumBlocksHigh) dead = true;

        // Comeu ela mesma?
        for (int i = m_SnakeLength - 1; i > 0; i--) {
            if ((i > 4) && (m_SnakeXs[0] == m_SnakeXs[i]) && (m_SnakeYs[0] == m_SnakeYs[i])) {
                dead = true;
            }
        }

        return dead;
    }

    public void updateGame() {
        // A cabeça da cobra tocou na maçã?
        if (m_SnakeXs[0] == m_AppleX && m_SnakeYs[0] == m_AppleY) {
            eatApple();
        }

        moveSnake();

        if (detectDeath()) {
            // Inicia novamente
            m_SoundPool.play(m_dead_sound, 1, 1, 0, 0, 1);

            startGame();
        }
    }

    public void drawGame() {
        // Preparando para desenhar
        if (m_Holder.getSurface().isValid()) {
            m_Canvas = m_Holder.lockCanvas();

            // Define a cor da tela
            m_Canvas.drawColor(Color.argb(255, 102, 204, 0));

            // Define a cor da cobra
            m_Paint.setColor(Color.argb(255, 20, 97, 44));


            // Define o tamanho da pontuação
            m_Paint.setTextSize(30);
            m_Canvas.drawText("Score:" + m_Score, 10, 30, m_Paint);


            //Desenha a cobra
            for (int i = 0; i < m_SnakeLength; i++) {
                m_Canvas.drawRect(m_SnakeXs[i] * m_BlockSize,
                        (m_SnakeYs[i] * m_BlockSize),
                        (m_SnakeXs[i] * m_BlockSize) + m_BlockSize,
                        (m_SnakeYs[i] * m_BlockSize) + m_BlockSize,
                        m_Paint);
            }


            // Define a cor da maçã
            m_Paint.setColor(Color.argb(255, 200, 0, 0));

            // Desenha a maçã
            m_Canvas.drawRect(m_AppleX * m_BlockSize,
                    (m_AppleY * m_BlockSize),
                    (m_AppleX * m_BlockSize) + m_BlockSize,
                    (m_AppleY * m_BlockSize) + m_BlockSize,
                    m_Paint);

            // Desenha a tela
            m_Holder.unlockCanvasAndPost(m_Canvas);
        }

    }

    public boolean checkForUpdate() {

        // Devemos atualizar o quadro
        if(m_NextFrameTime <= System.currentTimeMillis()){
            // O décimo de segundo passou

            // Configura quando a próxima atualização será ativada
            m_NextFrameTime =System.currentTimeMillis() + MILLIS_IN_A_SECOND / FPS;

            // Retorna verdadeiro para as funções de atualização e desenho
            // sejam executadas
            return true;
        }

        return false;
    }


    public void pause() {
        m_Playing = false;
        try {
            m_Thread.join();
        } catch (InterruptedException e) {
            // Erro
        }
    }

    public void resume() {
        m_Playing = true;
        m_Thread = new Thread(this);
        m_Thread.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {

        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:
                if (motionEvent.getX() >= m_ScreenWidth / 2) {
                    switch(m_Direction){
                        case UP:
                            m_Direction = Direction.RIGHT;
                            break;
                        case RIGHT:
                            m_Direction = Direction.DOWN;
                            break;
                        case DOWN:
                            m_Direction = Direction.LEFT;
                            break;
                        case LEFT:
                            m_Direction = Direction.UP;
                            break;
                    }
                } else {
                    switch(m_Direction){
                        case UP:
                            m_Direction = Direction.LEFT;
                            break;
                        case LEFT:
                            m_Direction = Direction.DOWN;
                            break;
                        case DOWN:
                            m_Direction = Direction.RIGHT;
                            break;
                        case RIGHT:
                            m_Direction = Direction.UP;
                            break;
                    }
                }
        }
        return true;
    }
}