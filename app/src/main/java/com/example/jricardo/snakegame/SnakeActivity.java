package com.example.jricardo.snakegame;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;

public class SnakeActivity extends Activity {

    // Declara a instância da SnakeView
    SnakeView snakeView;
    // Ela será iniciada no método onCreate
    // pois lá temos mais informações sobre o dispositivo do jogador =)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Descobrir a altura e largura da tela
        Display display = getWindowManager().getDefaultDisplay();

        // Carreha a resolução em um objeto do tipo Point
        Point size = new Point();
        display.getSize(size);

        // Cria uma nova View baseada na classe SnakeView
        snakeView = new SnakeView(this, size);

        // Define o objeto snakeView como sendo o view padrão da Activity
        setContentView(snakeView);
    }

    // Inicia a thread em snakeView quando esta Activity
    // é exibida para o jogador
    @Override
    protected void onResume() {
        super.onResume();
        snakeView.resume();
    }

    // Certifica-se de que a thread em snakeView está parada
    // se esta Activity vai ser fechada
    @Override
    protected void onPause() {
        super.onPause();
        snakeView.pause();
    }
}

